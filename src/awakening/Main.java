package awakening;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class Main {


    public static void main(String[]args) throws Exception{
        Connection connection = null;
        String url = "jdbc:mysql://localhost:3306/testdb?useSSL=false";
        //String user = "your_username";
        //String password = "your_password";
        Properties properties = new Properties();
        properties.setProperty("user", "your_username");
        properties.setProperty("password", "your_password");

        String suitableDriver = "com.mysql.jdbc.Driver";
        Class.forName(suitableDriver);
        try {
            connection = DriverManager.
                    getConnection(url, properties);
            System.out.println("Connection ready!");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}

